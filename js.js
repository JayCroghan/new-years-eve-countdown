var lTimeChange = 0;
var lFlashTimes = 20;
var lFlashTimer = 500;
var aZones = new Array();
aZones[0] = [ '19:00', '', 'Kiribati Line Islands (except U.S. dependencies - see UTC-11)', '' ];
aZones[1] = [ '18:45', '', 'New Zealand: Chatham Islands (during Daylight Saving Time)', '' ];
aZones[2] = [ '18:00', '', 'Kiribati: Phoenix Islands', 'New Zealand (during Daylight Saving Time)', 'South Pole (during Daylight Saving Time)', 'Tonga', '' ];
aZones[3] = [ '17:45', '', 'New Zealand: Chatham Islands', '' ];
aZones[4] = [ '17:00', '', 'Fiji', 'Kiribati: Gilbert Islands', 'Marshall Islands', 'Nauru', 'New Zealand', 'Russia: Chukotka, Kamchatka', 'South Pole', 'Tuvalu', 'Wake Island', 'Wallis and Futuna', '' ];
aZones[5] = [ '16:30', '', 'Australia: Norfolk Island', '' ];
aZones[6] = [ '16:00', '', 'Australia: Australian Capital Territory, New South Wales, Tasmania, Victoria (Australian Eastern Daylight Time)', 'Federated States of Micronesia: Kosrae, Pohnpei', 'New Caledonia', 'Russia: Kuril Islands, Magadan Oblast, Sakha', 'Solomon Islands', 'Vanuatu', '' ];
aZones[7] = [ '15:30', '', 'Australia: South Australia (Australian Central Daylight Time)', '' ];
aZones[8] = [ '15:00', '', 'Australia: Australian Capital Territory, New South Wales, Queensland, Tasmania, Victoria (Australian Eastern Standard Time)', 'Federated States of Micronesia: Chuuk, Yap', 'Guam', 'Northern Mariana Islands', 'Papua New Guinea', 'Russia: Primorsky Krai, Khabarovsk Krai, Yakutia, Sakhalin Island', '' ];
aZones[9] = [ '14:30', '', 'Australia: Northern Territory, South Australia (Australian Central Standard Time)', '' ];
aZones[10] = [ '14:00', '', 'East Timor', 'Indonesia (eastern): Maluku, Western New Guinea', 'Japan', 'North Korea', 'South Korea', 'Palau', 'Russia: Amur Oblast, Zabaykalsky Krai, Yakutia', '' ];
aZones[11] = [ '13:00', '', 'Australia: Western Australia (Australian Western Standard Time)', 'Brunei', 'China', 'Hong Kong', 'Indonesia (central): (Bali, Borneo)', 'Macau', 'Malaysia', 'Mongolia (most of country)', 'Philippines', 'Singapore', 'Taiwan', 'Russia: Buryatia, Irkutsk Oblast', '' ];
aZones[12] = [ '12:00', '', 'Bangladesh', 'Cambodia', 'Christmas Island (Australia)', 'Indonesia (western): (Java, Sumatra)', 'Laos', 'Mongolia (Hovd, Uvs, and Bayan-Ölgii)', 'Russia: Kemerovo, Khakassia, Krasnoyarsk Krai, Tomsk, Tuva', 'Thailand', 'Vietnam', '' ];
aZones[13] = [ '11:30', '', 'Cocos Islands', 'Myanmar', '' ];
aZones[14] = [ '11:00', '', 'Bhutan', 'British Indian Ocean Territory (CIA)', 'Kazakhstan (Eastern)', 'Kyrgyzstan', 'Russia: Altai Krai, Altai Republic, Novosibirsk Oblast, Omsk Oblast, Tomsk Oblast', '' ];
aZones[15] = [ '10:45', '', 'Nepal', '' ];
aZones[16] = [ '10:30', '', 'India', 'Sri Lanka', '' ];
aZones[17] = [ '10:00', '', 'British Indian Ocean Territory (NAO)', 'French Southern and Antarctic Lands', 'Heard Island and McDonald Islands', 'Kazakhstan (Western)', 'Maldives', 'Pakistan', 'Russia: Astrakhan, Bashkortostan, Chelyabinsk, Kurgan, Orenburg, Perm, Saratov, Sverdlovsk, Tyumen, Ulyanovsk, Volgograd', 'Tajikistan', 'Turkmenistan', 'Uzbekistan', '' ];
aZones[18] = [ '9:30', '', 'Afghanistan', '' ];
aZones[19] = [ '9:00', '', 'Armenia', 'Azerbaijan', 'Georgia', 'Mauritius', 'Oman', 'Réunion', 'Russia: Samara, Udmurtia', 'Seychelles', 'United Arab Emirates', '' ];
aZones[20] = [ '8:30', '', 'Iran', '' ];
aZones[21] = [ '8:00', '', 'Bahrain', 'Comoros', 'Djibouti', 'Eritrea', 'Ethiopia', 'Iraq', 'Kenya', 'Kuwait', 'Madagascar', 'Mayotte', 'Qatar', 'Russia: most of European portion, including Moscow, Saint Petersburg, Rostov on Don, Novaya Zemlya, Franz Josef Land, and all railroads throughout Russia', 'Saudi Arabia', 'Somalia', 'Sudan', 'Tanzania', 'Uganda', 'Yemen', '' ];
aZones[22] = [ '7:00', '', 'Belarus', 'Botswana', 'Bulgaria', 'Burundi', 'Democratic Republic of the Congo (eastern)', 'Cyprus', 'Egypt', 'Estonia', 'Finland', 'Greece', 'Israel', 'Jordan', 'Latvia', 'Lebanon', 'Lesotho', 'Libya', 'Lithuania', 'Malawi', 'Moldova', 'Mozambique', 'Romania', 'Russia: Kaliningrad Oblast', 'Rwanda', 'South Africa', 'Swaziland', 'Syria', 'Turkey', 'Ukraine', 'Zambia', 'Zimbabwe', '' ];
aZones[23] = [ '6:00', '', 'Albania', 'Andorra', 'Algeria', 'Angola', 'Austria', 'Belgium', 'Benin', 'Bosnia and Herzegovina', 'Cameroon', 'Central African Republic', 'Chad', 'Republic of the Congo', 'Democratic Republic of the Congo (western)', 'Croatia', 'Czech Republic', 'Denmark', 'Equatorial Guinea', 'France', 'Gabon', 'Germany', 'Gibraltar', 'Hungary', 'Italy', 'Liechtenstein', 'Luxembourg', 'Republic of Macedonia', 'Malta', 'Monaco', 'Montenegro', 'Namibia', 'Netherlands', 'Niger', 'Nigeria', 'Norway', 'Poland', 'San Marino', 'Serbia', 'Slovakia', 'Slovenia', 'Spain', 'Sweden', 'Switzerland', 'Tunisia', 'Vatican City', '' ];
aZones[24] = [ '5:00', '', 'Burkina Faso', 'Bouvet Island', 'Canary Islands', 'Cote d\'Ivoire', 'Faroe Islands', 'Gambia', 'Ghana', 'Greenland (northeastern)', 'Guernsey', 'Guinea', 'Guinea-Bissau', 'Iceland', 'Ireland', 'Isle of Man', 'Jersey', 'Liberia', 'Mali', 'Mauritania', 'Morocco', 'Northern Ireland', 'Portugal', 'Saint Helena', 'Senegal', 'Sierra Leone', 'Sao Tome and Principe', 'Togo', 'United Kingdom', 'Western Sahara', '' ];
aZones[25] = [ '4:00', '', 'Azores (Portugal)', 'Cape Verde', 'Greenland (east)', '' ];
aZones[26] = [ '3:00', '', 'Fernando de Noronha (Brazil)', 'South Georgia and the South Sandwich Islands', 'Trindade and Martim Vaz (uninhabited islands) (Brazil)', '' ];
aZones[27] = [ '2:00', '', 'Argentina', 'Brazil, Rio, São Paulo, Fortaleza, Maceio, Recife, Salvador etc.', 'French Guiana', 'Greenland (central)', 'Guyana', 'Saint Pierre and Miquelon', 'Suriname', 'Uruguay', '' ];
aZones[28] = [ '2:30', '', 'Canada', '' ];
aZones[29] = [ '1:00', '', 'Anguilla', 'Antigua and Barbuda', 'Aruba', 'Barbados', 'Bermuda', 'Bolivia', 'Brazil: Boa Vista, Campo Grande, Manaus', 'Canada, New Brunswick, Nova Scotia, Prince Edward Island, eastern Quebec', 'Chile', 'Dominica', 'Dominican Republic', 'Falkland Islands', 'Greenland (west)', 'Grenada', 'Guadeloupe', 'Guyana', 'Martinique', 'Montserrat', 'Netherlands Antilles', 'Paraguay', 'Puerto Rico', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Vincent and the Grenadines', 'Trinidad and Tobago', 'U.S. Virgin Islands', '' ];
aZones[30] = [ '0:30', '', 'Venezuela', '' ];
aZones[31] = [ '0:00', '', 'Bahamas', 'Brazil: Acre', 'Canada ("Eastern Time"): Nunavut, most of Ontario, most of Quebec', 'Cayman Islands', 'Colombia', 'Cuba', 'Ecuador', 'Haiti', 'Jamaica', 'Navassa Island', 'Panama', 'Peru', 'Turks and Caicos Islands', 'United States of America ("Eastern Time"): Maine, New Hampshire, Vermont, New York, Massachusetts, Connecticut, Rhode Island, Michigan except extreme northwestern counties, Indiana except the southwest and northwest corners, Ohio, Pennsylvania, New Jersey, eastern Kentucky, West Virginia, Virginia, Washington, D.C., Maryland, Delaware, eastern Tennessee, North Carolina, Georgia, South Carolina, Florida except western part of panhandle.', '' ];
aZones[32] = [ '-1:00', '', 'Belize', 'Canada ("Central Time"): Manitoba, Saskatchewan, north-western Ontario', 'Costa Rica', 'Easter Island', 'El Salvador', 'Galapagos Islands', 'Guatemala', 'Honduras', 'Mexico', 'Nicaragua', 'United States of America ("Central Time"): Wisconsin, Illinois, the southwest and northwest corners of Indiana, western Kentucky, western and middle Tennessee, Mississippi, Alabama, Minnesota, Iowa, Missouri, Arkansas, Louisiana, north and east North Dakota, eastern South Dakota, middle and eastern Nebraska, most of Kansas, Oklahoma, most of Texas, part of western Florida(panhandle).', '' ];
aZones[33] = [ '-2:00', '', 'Canada ("Mountain Time"): Alberta, small eastern portion of British Columbia', 'Mexico: Baja California Sur: Chihuahua, Nayarit, Sinaloa, Sonora', 'United States of America ("Mountain Time"): southwest North Dakota, western South Dakota, western Nebraska, a sliver of Kansas, Montana, a sliver of Oregon, southern Idaho, Wyoming, Utah, Colorado, Arizona, New Mexico, a corner of Texas', '' ];
aZones[34] = [ '-3:00', '', 'Canada ("Pacific Time"): most of British Columbia, Yukon', 'Clipperton Island', 'Mexico: Baja California Norte', 'Pitcairn Islands', 'United States of America ("Pacific Time"): Washington, northern Idaho, most of Oregon, California, Nevada', '' ];
aZones[35] = [ '-4:00', '', 'French Polynesia: Gambier Islands', 'United States of America: most of Alaska', '' ];
aZones[36] = [ '-4:30', '', 'French Polynesia: Marquesa Islands', '' ];
aZones[37] = [ '-5:00', '', 'Cook Islands', 'French Polynesia: Society Islands, Tuamotu Islands, Austral Islands', 'Johnston Atoll', 'Tokelau', 'United States of America: Hawaii, and Aleutian Islands in Alaska', '' ];
aZones[38] = [ '-6:00', '', 'American Samoa', 'Jarvis Island, Kingman Reef, Palmyra Atoll', 'Midway Islands', 'Niue', 'Samoa', '' ];
aZones[39] = [ '-7:00', '', 'Baker Island', 'Howland Island' ];

function parseTime(s)
{
	var c = s.split(':');
	return parseInt(c[0]) * 60 + parseInt(c[1]);
}

var currentZone = 0;

function fireTimer( newZone )
{
	$('#countdown_dashboard .digit').html('<div class="top"></div><div class="bottom"></div>');
	dashChangeTo( 'countdown_dashboard', 'hours_dash', 0, 0);
	
	var midnight = 1440;
	if (!( newZone === undefined ))
	{
		
		var zone = aZones[newZone][0].split(':');
		zoneHours = Number(zone[0]) - lTimeChange; // TODO
		zoneMins = Number(zone[1]);
		var curHours = zoneHours;
		var curMins = curHours * 60 + zoneMins; //60 - zoneMins;
		var startTime = midnight - curMins;
		currentZone = newZone;

		var now = new Date();
		var hours = now.getHours();
		var minutes = now.getMinutes();
		var seconds = now.getSeconds();
		nowMins = ( hours * 60 ) + minutes;
		nowSeconds = ( nowMins * 60 + seconds );
		
		doTick( zone, startTime, nowSeconds, true );
		return;
	}
	var x = 0;
	
	// Now in minutes.
	var now = new Date();
	var hours = now.getHours();
	var minutes = now.getMinutes();
	var nowMins = (hours * 60) + minutes;
	var nowSeconds = (nowMins * 60 + now.getSeconds());
	
	var zoneHours = 0;
	var zoneMins = 0;
	var zone = '';
	for ( x = 0; x < 39; x++ )
	{
		// Get minutes before midnight this timezone hits midnight.
		var zone = aZones[x][0].split(':');
		zoneHours = Number(zone[0]) - lTimeChange; // TODO
		zoneMins = Number(zone[1]);
		var curHours = zoneHours;
		var curMins = curHours * 60 + zoneMins; //60 - zoneMins;
		
		// Get the time this midnight will occur.
		var startTime = midnight - curMins;
		
		// If now is less than the time we want to start.
		if ( nowSeconds < ( startTime * 60 ))
		{
			curMins = 60 - zoneMins;
			curHours = 24 - zoneHours;
			currentZone = x;
			doTick( zone, startTime, nowSeconds, true );
			return;
		}
	}
	return;
}

function doTick( zone, startTime, nowSeconds, first_time )
{
	
	var zoneHours = ( Number(zone[0]) - lTimeChange )  * -1; // TODO
	var zoneMins = Number(zone[1]);
	
	
	var startSeconds = (startTime * 60) - 1;
	var secondsToGo = startSeconds - nowSeconds;
	
	var theTime = secondsToTime( secondsToGo );
	
	$('#timer').html(pad(theTime["h"],2) + ':' + pad(theTime["m"],2) + ':' + pad(theTime["s"],2));
	
	var minsCount = parseInt( theTime["m"] );
	var secsCount = parseInt( theTime["s"] );
	
	/*
	$('.hours_dash .digit:first').html('0');
	$('.hours_dash .digit:last').html('0');

    	$('.minutes_dash .digit:first').html(minsCount[0]);
	$('.minutes_dash .digit:last').html(minsCount[1]);
	
	$('.seconds_dash .digit:first').html(secsCount[0]);
	$('.seconds_dash .digit:last').html(secsCount[1]);
	
	*/
       
	dashChangeTo( 'countdown_dashboard', 'seconds_dash', secsCount, 800);
	dashChangeTo( 'countdown_dashboard', 'minutes_dash', minsCount, 1200);
		
	bFinished = false;
	if ( secondsToGo <= 0 )
	{
		bFinished = true;
	}
	if ( first_time )
	{
		var x = 0;
		var sCities = 0;
		for ( x = 2; x < (aZones[currentZone].length - 1); x++ )
		{
			var aCities = ['Ireland', 'Argentina', 'Netherlands', 'United Kingdom', 'Peru', 'Canada', 'Brazil', 'Bolivia', 'Chile', 'Colombia', 'Canada ("Eastern Time"): Nunavut, most of Ontario, most of Quebec'];
			sCity = aZones[currentZone][x];
			/*
			if ( jQuery.inArray( sCity, aCities ) > -1 )
			{
				sCity = '<span class="special_country">' + sCity + '</span>'
			}
			*/
			if ( sCity.length > 0 )
			{
				if ( x === 2 )
				{
					sCities = sCity;
				}
				else
				{
					sCities = sCities + ', ' + sCity;
				}
			}
		}
		$('#country_list').html(sCities);
	}
	if ( bFinished )
	{
		finishSequence( 0 );	
	}
	else
	{
		var now = new Date();
		var hours = now.getHours();
		var minutes = now.getMinutes();
		var seconds = now.getSeconds();
		nowMins = ( hours * 60 ) + minutes;
		nowSeconds = ( nowMins * 60 + seconds );
		setTimeout( function() { doTick( zone, startTime, nowSeconds, false ); }, 1000);
	}
}

function dashChangeTo(id, dash, n, duration)
{
	$this = $('#' + id);
	d2 = n%10;
	d1 = (n - n%10) / 10;

	if ($('#' + $this.attr('id') + ' .' + dash))
	{
		digitChangeTo('#' + $this.attr('id') + ' .' + dash + ' .digit:first', d1, duration);
		digitChangeTo('#' + $this.attr('id') + ' .' + dash + ' .digit:last', d2, duration);
	}
};
	
function digitChangeTo(digit, n, duration)
{
	if (!duration)
	{
		duration = 800;
	}
	if ($(digit + ' div.top').html() != n + '')
	{

		$(digit + ' div.top').css({'display': 'none'});
		$(digit + ' div.top').html((n ? n : '0')).slideDown(duration);

		$(digit + ' div.bottom').animate({'height': ''}, duration, function() {
			$(digit + ' div.bottom').html($(digit + ' div.top').html());
			$(digit + ' div.bottom').css({'display': 'block', 'height': ''});
			$(digit + ' div.top').hide().slideUp(10);


		});
	}
};

function finishSequence( times )
{
	if ( times === lFlashTimes )
	{
		$('#timer').css('color', '#FFFFFF');
		fireTimer( ( currentZone + 1 ));
	}
	else
	{
		if ( times === 0 )
		{
			$('#timer').css('color', '#FF0000');
		}
		$('#timer').toggle();
		$('#country_list').toggle();
		setTimeout( function() { finishSequence( times + 1 ); }, lFlashTimer );
	}
}

function pad(n, len)
{
   
	s = n.toString();
	if (s.length < len) 
	{
		s = ('0000000000' + s).slice(-len);
	}

	return s;
   
}

/**
 * Convert number of seconds into time object
 *
 * @param secs Number of seconds to convert
 * @return object
 */
function secondsToTime( secs )
{
	// 60 *60 = 3600
	var hours = Math.floor( secs / 3600 );
	
	var divisor_for_minutes = secs % 3600;
	var minutes = Math.floor(divisor_for_minutes / 60);

	var divisor_for_seconds = divisor_for_minutes % 60;
	var seconds = Math.ceil(divisor_for_seconds);
	
	var obj = {
		"h": hours,
		"m": minutes,
		"s": seconds
	};
	return obj;
}

$(document).ready(function()
{
    $('#time_zone').change( function() 
    {
	lTimeChange = parseInt( $(this).find("option:selected").val() );
	fireTimer();
	$("#initial_container").hide();
	$("#timer_container").show();
    });
});